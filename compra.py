#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

habitual = ("patatas", "leche", "pan")
especifica = []


def main():
    seguir = True

    while seguir:
        compra = input("Elemento a comprar: ")
        if not habitual.__contains__(compra) and not especifica.__contains__(compra):
            if not compra == "":
                especifica.append(compra)
            else:
                seguir = False

    print("Lista de la compra:")
    for articulo in habitual:
        print(articulo)
    for articulo in especifica:
        print(articulo)

    print("Elementos habituales:", len(habitual))
    print("Elementos específicos:", len(especifica))
    print("Elementos en lista:", len(habitual) + len(especifica))


if __name__ == '__main__':
    main()
